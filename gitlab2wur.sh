#!/bin/bash
# Gitlab to WUR git copy script
# Jan van Haarst
# 20150116

# Needs SSH connection to work
# Also needs python3, curl and git

# Debug
#set -o xtrace
#set -o verbose

# Stop on error
set -o errexit

# TOKENS (can be found under "Profile settings" -> "Account")
WUR_token=''
GITLAB_token=''
if [ "$WUR_token" == '' ] || [ "$GITLAB_token" == '' ]
then
    if [ "$WUR_TOKEN" != '' ] && [ "$GITLAB_TOKEN" != '' ]
    then
        WUR_token=$WUR_TOKEN
        GITLAB_token=$GITLAB_TOKEN
    else
        echo "Please enter a gitlab and WUR token in the script, or run it like this:"
        echo "GITLAB_TOKEN=( token from Gitlab ) WUR_TOKEN=( token from WUR )  " $(basename $0)
        exit 1
    fi
fi
# Temporary directory 
TEMPDIR=`echo /tmp/gitmirror.$$`

# Servers
WUR="https://git.wageningenur.nl/"
GITLAB="https://gitlab.com"

# The default grep in OSX doesn't support pcre, so we switch to pcregrep
# http://stackoverflow.com/questions/592620/check-if-a-program-exists-from-a-bash-script
if hash pcregrep 2>/dev/null
then
    GREP="pcregrep" # Mac
else
    GREP="grep -P" # Linux
fi

# Get the user repo's
# To be sure we get all repo's, we loop a couple of times, and stop when empty
list_of_repos=''
for counter in `seq 1 100`
do
    # We need to allow errors here, as the grep will return an errorcode if no match is found
    set +o errexit
    repos=$(curl -s "${GITLAB}/api/v3/projects?private_token=${GITLAB_token}&per_page=100&page=${counter}" | python3 -m json.tool | $GREP -o '(?<="ssh_url_to_repo": ")[^"]*')
    # Stop on error
    set -o errexit
    if [ "$repos" == '' ]
    then
        break
    fi
    list_of_repos="$list_of_repos $repos"
done

# Mirror to temporary directory, create new repo in WUR, en push.
mkdir -p $TEMPDIR
cd $TEMPDIR
for repo in $list_of_repos
do
    name=`echo $repo  | cut -f 2 -d '/' | sed 's/.git//'`
    # We need to allow errors here, as the grep will return an errorcode if no match is found (repo already exists)
    set +o errexit
    new_repo=$(curl -s -H "Content-Type:application/json" ${WUR}/api/v3/projects?private_token=$WUR_token --data "{ \"name\": \"$name\" }" |  python3 -m json.tool |  $GREP -o '(?<="ssh_url_to_repo": ")[^"]*')
    set -o errexit
    if [ "$new_repo" == '' ]
    then
        echo "$name already exists!"
        rm -rf $name
        continue
    fi
    git clone --mirror $repo $name
    cd $name
    git push --mirror $new_repo
    cd - > /dev/null
    rm -rf $name
done
rmdir $TEMPDIR
