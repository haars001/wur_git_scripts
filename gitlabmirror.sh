#!/bin/bash
# Gitlab mirror script
# Jan van Haarst
# 20150217

# Needs SSH connection to work
# Also needs python3, curl and git

# Debug
# set -o xtrace
# set -o verbose

# Stop on error
set -o errexit

# TOKENS (can be found under "Profile settings" -> "Account")
GITLAB_token=''
if [ "$GITLAB_token" == '' ]
then
    GITLAB_token=$GITLAB_TOKEN
else
    echo "Please enter a gitlab token in the script, or run it like this:"
    echo "GITLAB_TOKEN=( token from Gitlab ) " $(basename $0)
    exit 1
fi

# Servers
GITLAB="https://git.wageningenur.nl/"

# The default grep in OSX doesn't support pcre, so we switch to pcregrep
# http://stackoverflow.com/questions/592620/check-if-a-program-exists-from-a-bash-script
if hash pcregrep 2>/dev/null
then
    GREP="pcregrep" # Mac
else
    GREP="grep -P" # Linux
fi

# Get the user repo's
# To be sure we get all repo's, we loop a couple of times, and stop when empty
list_of_repos=''
for counter in `seq 1 100`
do
    # We need to allow errors here, as the grep will return an errorcode if no match is found
    set +o errexit
    repos=$(curl -s "${GITLAB}/api/v3/projects?private_token=${GITLAB_token}&per_page=100&page=${counter}" | python3 -m json.tool | $GREP -o '(?<="ssh_url_to_repo": ")[^"]*')
    # Stop on error
    set -o errexit
    if [ "$repos" == '' ]
    then
        break
    fi
    list_of_repos="$list_of_repos $repos"
done

# Mirror to current directory
for repo in $list_of_repos
do
    # We need to allow errors here, as git will return an errorcode if a repo is already there
    set +o errexit
    git clone --mirror $repo
    # Stop on error
    set -o errexit
done
# Now we can do an update on the ones we already have or just got
for dir in `find . -maxdepth 1 -type d -name '*.git'`
do
    pushd "$dir" > /dev/null
    git remote update
    popd > /dev/null
done

