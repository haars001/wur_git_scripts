This repository contains a couple of scripts to make living with the WUR git repo a bit easier.

* `github2wur.sh` Export repositories from GitHub, and import them into the WUR repository. It only imports personal and public repositories. (not needed anymore, you can now import directly from Github inside Gitlab)
* `gitlab2wur.sh` Export repositories from gitlab.com, and import them into the WUR repository.

